package com.mortgage;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.mortgage.controller.MortgageController;
import com.mortgage.domain.MortgageCheck;
import com.mortgage.domain.MortgageCheckResponse;
import com.mortgage.domain.MortgageRate;
import com.mortgage.response.Response;
import com.mortgage.services.MortgageService;

/**
 * @author Arvind
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = MortgageController.class, secure = false)
public class MortgageControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MortgageService mortgageService;

	@MockBean
	private Response response;

	/**
	 * Test method for
	 * {@link com.mortgage.controller.MortgageController#getMortgageList()}.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetMortgageList() throws Exception {

		List<MortgageRate> mortgageDetailsMock = new ArrayList<>();
		mortgageDetailsMock.add(new MortgageRate(10, 11.25f, 1512913194729L));
		mortgageDetailsMock.add(new MortgageRate(15, 10.25f, 1512913183441L));
		mortgageDetailsMock.add(new MortgageRate(20, 9.25f, 1512913220058L));

		String expectedmortgageDetailsJson = "[\r\n" + "    {\r\n" + "        \"maturityPeriod\": 10,\r\n"
				+ "        \"interestRate\": 11.25,\r\n" + "        \"lastUpdate\": 1512913194729\r\n" + "    },\r\n"
				+ "    {\r\n" + "        \"maturityPeriod\": 15,\r\n" + "        \"interestRate\": 10.25,\r\n"
				+ "        \"lastUpdate\": 1512913183441\r\n" + "    },\r\n" + "    {\r\n"
				+ "        \"maturityPeriod\": 20,\r\n" + "        \"interestRate\": 9.25,\r\n"
				+ "        \"lastUpdate\": 1512913220058\r\n" + "    }\r\n" + "]";

		Mockito.when(mortgageService.getMortgageList()).thenReturn(mortgageDetailsMock);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/interest-rate")
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals(expectedmortgageDetailsJson, result.getResponse().getContentAsString(), false);
	}

	/**
	 * This is the happy flow cases for mortgage eligibility. Both Business
	 * rules satisfied
	 * 
	 * Rule 1 : Mortgage must not exceed 4 times income Rule 2 : Mortgage must
	 * not exceed homeValue(135001)
	 * 
	 * Expected outcome: feasible must be false
	 * 
	 * PS: To about Mortgage calculate and monthlycostIncome, please see the
	 * ReadMe.
	 */
	@Test
	public void testCheckMortgageValid() throws Exception {

		String expectedString = "{\r\n" + "    \"feasible\": true,\r\n" + "    \"monthlyMortgage\": 11250\r\n" + "}";

		MortgageCheck mortgageCheck = new MortgageCheck();
		mortgageCheck.setHomeValue(30000);
		mortgageCheck.setIncome(1000);
		mortgageCheck.setLoanValue(2);
		mortgageCheck.setMaturityPeriod(20);

		MortgageCheckResponse mortgageCheckResponse = new MortgageCheckResponse();
		mortgageCheckResponse.setFeasible(true);
		mortgageCheckResponse.setMonthlyMortgage(11250);

		Mockito.when(mortgageService.getMortgageCheckResponse(Mockito.any(MortgageCheck.class)))
				.thenReturn(mortgageCheckResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/mortgage-check")
				.accept(MediaType.APPLICATION_JSON).param("income", "100000").param("maturityPeriod", "10")
				.param("loanValue", "100").param("homeValue", "135000");

		/** Calculate the value of mortgage */
		double mortgage = 100 * 11.25 * (10 * 12);
		double income = 100000;
		Assert.assertTrue(
				"Rule 1 Matched: Mortgage : " + mortgage + ": is less than equal to 4 time income: " + 4 * income,
				mortgage <= 4 * income);

		double homeValue = 135000;
		Assert.assertTrue("Rule 2 Matched: Mortgage :" + mortgage + ": is less than equal to home Amount :" + homeValue,
				mortgage <= homeValue);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals(expectedString, result.getResponse().getContentAsString(), false);

	}

	/**
	 * Rule 1 is not Satisfied.Mortgage must not exceed 4 times income
	 * 
	 * Expected outcome: feasible must be false. PS: To about Mortgage calculate
	 * and monthlycostIncome, please see the ReadMe.
	 */
	@Test
	public void testRuleOneFailed() throws Exception {
		String expectedString = "{\r\n" + "    \"feasible\": false,\r\n" + "    \"monthlyMortgage\": 11250\r\n" + "}";

		MortgageCheck mortgageCheck = new MortgageCheck();
		mortgageCheck.setHomeValue(30000);
		mortgageCheck.setIncome(1000);
		mortgageCheck.setLoanValue(2);
		mortgageCheck.setMaturityPeriod(20);

		MortgageCheckResponse mortgageCheckResponse = new MortgageCheckResponse();
		mortgageCheckResponse.setFeasible(false);
		mortgageCheckResponse.setMonthlyMortgage(11250);

		/** Calculate the value of mortgage */
		double mortgage = 100 * 11.25 * (10 * 12);
		double income = 10000;
		Assert.assertFalse(
				"Rule 1 Not Matched: Mortgage :" + mortgage + " is greater than equal to 4 time income: " + 4 * income,
				mortgage <= 4 * income);

		Mockito.when(mortgageService.getMortgageCheckResponse(Mockito.any(MortgageCheck.class)))
				.thenReturn(mortgageCheckResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/mortgage-check")
				.accept(MediaType.APPLICATION_JSON).param("income", "10000").param("maturityPeriod", "10")
				.param("loanValue", "100").param("homeValue", "135000");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals(expectedString, result.getResponse().getContentAsString(), false);
	}

	/**
	 * Rule 2 is not Satisfied: Mortgage must not exceed homeValue
	 * 
	 * Expected outcome: feasible must be false
	 * 
	 * PS: To about Mortgage calculate and monthlycostIncome, please see the
	 * ReadMe.
	 */
	@Test
	public void testRuleSecondFailed() throws Exception {

		String expectedString = "{\r\n" + "    \"feasible\": false,\r\n" + "    \"monthlyMortgage\": 11250\r\n" + "}";

		MortgageCheck mortgageCheck = new MortgageCheck();
		mortgageCheck.setHomeValue(30000);
		mortgageCheck.setIncome(1000);
		mortgageCheck.setLoanValue(2);
		mortgageCheck.setMaturityPeriod(20);

		MortgageCheckResponse mortgageCheckResponse = new MortgageCheckResponse();
		mortgageCheckResponse.setFeasible(false);
		mortgageCheckResponse.setMonthlyMortgage(11250);

		Mockito.when(mortgageService.getMortgageCheckResponse(Mockito.any(MortgageCheck.class)))
				.thenReturn(mortgageCheckResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/mortgage-check")
				.accept(MediaType.APPLICATION_JSON).param("income", "100000").param("maturityPeriod", "10")
				.param("loanValue", "100").param("homeValue", "134999");

		double mortgage = 100 * 11.25 * (10 * 12);
		double homeValue = 134999;
		Assert.assertFalse("Rule 2 Not Matched: Mortgage :" +mortgage+ " is greater home Amount : "+homeValue, mortgage <= homeValue);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals(expectedString, result.getResponse().getContentAsString(), false);
	}
}